/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/27 16:58:10 by lburlach          #+#    #+#             */
/*   Updated: 2017/11/10 16:23:17 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Append a copy of the null-terminated string s2 to the end of the
** null-terminated string s1, then add a terminating '\0'.
** the string s1 must have sufficient space to hold the result.
*/

char	*ft_strcat(char *s1, const char *s2)
{
	size_t len1;
	size_t i;

	i = 0;
	len1 = ft_strlen(s1);
	while (s2[i])
		s1[len1++] = s2[i++];
	s1[len1] = '\0';
	return (s1);
}
