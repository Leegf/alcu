/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 19:11:50 by lburlach          #+#    #+#             */
/*   Updated: 2017/12/06 18:12:57 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Iterates a list lst and applies the function f to each link to create
** a “fresh” list (using malloc(3)) resulting from the successive
** applications of f. If the allocation fails, the function returns NULL.
*/

static int		pu_b(t_list **tmp, void *data, size_t con_s)
{
	t_list *hm;
	t_list *kek;

	if (!tmp)
		return (1);
	kek = *tmp;
	hm = ft_lstnew((void *)data, con_s);
	if (hm == NULL)
		return (1);
	if (!(*tmp))
	{
		*tmp = hm;
		return (0);
	}
	while (kek->next)
		kek = kek->next;
	kek->next = hm;
	return (0);
}

t_list			*ft_lstmap(t_list *lst, t_list *(f)(t_list *elem))
{
	t_list *fresh_list;
	t_list *head;
	t_list *prev;

	fresh_list = NULL;
	if (!lst || pu_b(&fresh_list, f(lst)->content, f(lst)->content_size) || !f)
		return (NULL);
	head = fresh_list;
	lst = lst->next;
	while (lst)
	{
		if (pu_b(&fresh_list, f(lst)->content, f(lst)->content_size))
		{
			while (head)
			{
				prev = head;
				head = head->next;
				free(prev);
			}
			fresh_list = NULL;
			return (NULL);
		}
		lst = lst->next;
	}
	return (fresh_list);
}
