/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 12:31:32 by lburlach          #+#    #+#             */
/*   Updated: 2017/11/10 16:25:37 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The strstr() function locates the first occurrence of the
** null-terminated string needle in the null-terminated string haystack.
** If needle is an empty string, haystack is returned;
** if needle occurs nowhere in haystack, NULL is returned;
** otherwise a pointer to the first character of the first occurrence of
** needle is returned.
*/

char	*ft_strstr(const char *haystack, const char *needle)
{
	size_t i;
	size_t k;
	size_t j;
	size_t len;

	len = ft_strlen(needle);
	i = 0;
	k = 0;
	while (haystack[i])
	{
		k = i;
		j = 0;
		while (haystack[k] == needle[j] && needle[j])
		{
			k++;
			j++;
		}
		if (j == len)
			return ((char *)&haystack[i]);
		i++;
	}
	return (*haystack == '\0' && *needle == '\0') ? (char *)haystack : (NULL);
}
