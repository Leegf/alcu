/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/30 18:18:01 by lburlach          #+#    #+#             */
/*   Updated: 2017/11/10 16:24:30 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Applies the function f to each character of the string passed as
** argument by giving its index as first argument to create a “fresh” new
** string (with malloc(3)) resulting from the suc- cessive applications of f.
*/

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char			*mapped_string;
	unsigned int	i;
	size_t			len_of_initial_str;

	i = 0;
	if (!s || !f)
		return (NULL);
	len_of_initial_str = ft_strlen(s);
	mapped_string = ft_strnew(len_of_initial_str);
	if (mapped_string == NULL)
		return (NULL);
	while (i < len_of_initial_str)
	{
		mapped_string[i] = f(i, s[i]);
		i++;
	}
	return (mapped_string);
}
