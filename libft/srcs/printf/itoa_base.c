/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   itoa_base.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 11:04:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/01/19 11:04:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static	size_t		c_s(long n, int base, size_t *tens)
{
	size_t count;

	count = 0;
	while (n / base >= 1)
	{
		*tens *= base;
		n /= base;
		count++;
	}
	return (count);
}

static	size_t		c_s_2(size_t n, int base, size_t *tens)
{
	size_t count;

	count = 0;
	while (n / base >= 1)
	{
		*tens *= base;
		n /= base;
		count++;
	}
	return (count);
}

char				*ft_itoa_base(long n, int b)
{
	char			*res;
	int				sign;
	long			n_cp;
	size_t			tens;
	char			*base_d;

	n_cp = n;
	base_d = ft_strdup("0123456789abcdef");
	sign = n_cp < 0 ? 1 : 0;
	tens = 1;
	n = 0;
	if (sign == 1)
		n_cp *= -1;
	res = n_cp == 0 ? ft_strnew(1) : ft_strnew(c_s(n_cp, b, &tens) + sign + 1);
	res[sign == 1 ? n++ : n] = '-';
	while (tens >= 1)
	{
		res[n++] = base_d[n_cp / tens];
		n_cp %= tens;
		tens /= b;
	}
	ft_strdel(&base_d);
	return (res);
}

char				*ft_u_itoa_base(size_t n, int b, int flag)
{
	char			*res;
	size_t			n_cp;
	size_t			tens;
	char			*base_d;

	n_cp = n;
	if (flag)
		base_d = ft_strdup("0123456789ABCDEF");
	else
		base_d = ft_strdup("0123456789abcdef");
	tens = 1;
	n = 0;
	res = n_cp == 0 ? ft_strnew(1) : ft_strnew(c_s_2(n_cp, b, &tens) + 1);
	while (tens >= 1)
	{
		res[n++] = base_d[n_cp / tens];
		n_cp %= tens;
		tens /= b;
	}
	ft_strdel(&base_d);
	return (res);
}

/*
** Hoping no one would ever find this mess...
*/

void				to_exit_2(t_flags flags, t_list **head, char *n, int s)
{
	char sign;

	sign = n[0] == '-' ? '-' : '+';
	if (flags.prec <= s && (flags.conv == 'o' || flags.conv == 'O') &&
		flags.hash)
		ft_lst_push_back(head, "0", 1);
	else if (flags.prec <= s && (flags.conv == 'x' || flags.conv == 'X'
								|| flags.conv == 'p') && !(flags.zero &&
			flags.width - count_that_shit2(flags, sign, s, n) > 0 &&
			flags.prec == -1 && !flags.minus) && !fk_p(flags, sign))
		hex_fix(&flags, head, n);
	if (!(flags.conv == 'p' && n[0] == '0' &&
		(flags.prec == 0 || (flags.point && flags.prec == -1))))
		ft_lst_push_back(head, n, s);
}
