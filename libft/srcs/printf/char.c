/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   char.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/17 20:14:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/01/17 20:14:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Make a lot of spaces of zeros depending on width's parameter.
*/

void	form_width(t_flags flags, t_list **head)
{
	char	*str;
	int		i;
	char	chr;

	i = 0;
	chr = flags.zero == 1 && !flags.minus ? '0' : ' ';
	str = ft_memalloc(flags.width);
	while (i < flags.width)
		str[i++] = chr;
	ft_lst_push_back(head, str, flags.width - 1);
	ft_strdel(&str);
}

/*
** Putting char into t_list seems like not the most easy task on the Earth.
*/

void	form_da_c_for_lst(char c, t_list **head)
{
	char *rez;

	rez = ft_memalloc(1);
	rez[0] = c;
	ft_lst_push_back(head, rez, 1);
	ft_strdel(&rez);
}

/*
** Counting bits in the char using simple method provided by Julia.
*/

int		count_those_great_bits(unsigned int c)
{
	int i;

	i = 0;
	while (c >= 1)
	{
		c /= 2;
		i++;
	}
	return (i);
}

/*
** Usual writing of char.
*/

void	write_char(t_flags *flags, int c, t_list **head)
{
	if ((*flags).length == 4 || (char)(*flags).conv == 'C')
	{
		write_l_char(flags, (unsigned int)c, head);
		return ;
	}
	if ((*flags).minus)
	{
		form_da_c_for_lst((char)c, head);
		form_width((*flags), head);
	}
	else
	{
		form_width((*flags), head);
		form_da_c_for_lst((char)c, head);
	}
}
