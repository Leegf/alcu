/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_merge.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 15:34:06 by lburlach          #+#    #+#             */
/*   Updated: 2017/12/06 18:05:54 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** It'll merge two lists into a single one.
*/

void	ft_lst_merge(t_list **alst, t_list *lst)
{
	if (!alst || !(*alst))
		return ;
	while (lst)
	{
		ft_lst_push_back(alst, lst->content, lst->content_size);
		lst = lst->next;
	}
}
