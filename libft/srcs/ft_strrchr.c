/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 12:10:54 by lburlach          #+#    #+#             */
/*   Updated: 2017/11/10 16:25:22 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The strrchr() function is identical to strchr(), except it locates the
** last occurrence of c.
*/

char	*ft_strrchr(const char *s, int c)
{
	char	tmp;
	size_t	i;

	i = 0;
	tmp = (char)c;
	while (*s)
	{
		s++;
		i++;
	}
	while (i > 0)
	{
		if (*s == tmp)
			return ((char *)s);
		s--;
		i--;
	}
	if (*s == tmp)
		return ((char *)s);
	return (NULL);
}
