NAME = alum1

SRC_DIR = ./srcs/
OBJ_DIR = ./obj/
INC_DIR = ./includes/
LIB_DIR = ./

SRC =	main.c \
		create_board.c \
		create_board2.c \
		game.c \
		ai.c \
		hero.c \
		print_board.c \

OBJ = $(addprefix $(OBJ_DIR), $(SRC:.c=.o))

INC = $(INC_DIR)$(NAME).h

LIBFT = $(LIB_DIR)libft/libft.a
LIBFT_INC = $(LIB_DIR)libft/includes/
LIBFT_FLAGS = -lft -L $(LIB_DIR)libft/

CFLAGS = -Wall -Wextra -Werror -g
#CFLAGS = -g
HFLAGS = -I $(INC_DIR) -I $(LIBFT_INC)
LFLAGS = $(LIBFT_FLAGS)

CC = gcc

all:
	make -C $(LIB_DIR)libft/
	make $(NAME)

$(NAME): $(OBJ) $(INC) $(LIBFT)
	$(CC) $(OBJ) $(LFLAGS) -o $(NAME)

$(OBJ_DIR)%.o: $(SRC_DIR)%.c $(INC)
	$(CC) $(CFLAGS) $(HFLAGS) -c $< -o $@

$(OBJ): | $(OBJ_DIR)

$(OBJ_DIR):
	mkdir $(OBJ_DIR)

clean:
	/bin/rm -f $(OBJ)
	make clean -C $(LIB_DIR)libft/

fclean: clean
	/bin/rm -f $(NAME)
	/bin/rm -rf $(OBJ_DIR)
	make fclean -C $(LIB_DIR)libft/

re: fclean all

.PHONY: all clean fclean re
