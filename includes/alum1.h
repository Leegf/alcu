/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alum.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amelihov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 11:50:22 by amelihov          #+#    #+#             */
/*   Updated: 2018/02/18 15:04:16 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ALUM1_H
# define ALUM1_H

# include "../libft/srcs/includes/ft_printf.h"
# include <fcntl.h>

# define MAX_MATCH 3

typedef struct	s_board
{
	short	*rows;
	size_t	nrows;
}				t_board;

t_board			create_board(int argc, char *argv[], t_list **head);
void			print_board(t_board board);
void			run_game(t_board board);
void			*init_ai(t_board board);
void			deinit_ai(void);
short			ai(t_board board, short row);
short			hero(t_board board, short row);
void			read_from_zero(t_board *tmp, t_list **head);
void			print_board(t_board board);
void			free_everything(t_board *tmp, t_list **head, char *tmp2,
				int f);

#endif
