/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ai.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amelihov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 15:12:32 by amelihov          #+#    #+#             */
/*   Updated: 2018/02/18 13:34:10 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum1.h"

static short	**get_targets_addr(void)
{
	static short	*targets;

	return (&targets);
}

#define WIN 1
#define LOSE 0

static short	choose_target(short prev_n, short prev_target)
{
	short	curr_target;

	if (prev_target == WIN)
		curr_target = ((prev_n - 1) % (MAX_MATCH + 1) != 0) ? WIN : LOSE;
	else
		curr_target = (prev_n % (MAX_MATCH + 1) != 0) ? WIN : LOSE;
	return (curr_target);
}

void			*init_ai(t_board board)
{
	short	**targets_addr;
	size_t	i;

	targets_addr = get_targets_addr();
	if (!(*targets_addr = malloc(sizeof(*board.rows) * board.nrows)))
		return (NULL);
	(*targets_addr)[0] = WIN;
	i = 1;
	while (i < board.nrows)
	{
		(*targets_addr)[i] = choose_target(board.rows[i - 1],
							(*targets_addr)[i - 1]);
		i++;
	}
	return ("");
}

void			deinit_ai(void)
{
	short	**targets_addr;

	targets_addr = get_targets_addr();
	free(*targets_addr);
	*targets_addr = NULL;
}

#define WIN_STRATEGY(n) (n - (MAX_MATCH + 1) * ((n - 1) / (MAX_MATCH + 1)) - 1)
#define LOSE_STRATEGY(n) (n - (MAX_MATCH + 1) * (n / (MAX_MATCH + 1)))

short			ai(t_board board, short curr_row)
{
	short	*targets;
	short	nmatches;

	targets = *get_targets_addr();
	nmatches = 0;
	if (targets[curr_row] == WIN)
		nmatches = WIN_STRATEGY(board.rows[curr_row]);
	else if (targets[curr_row] == LOSE)
		nmatches = LOSE_STRATEGY(board.rows[curr_row]);
	if (nmatches == 0)
		nmatches = 1;
	return (nmatches);
}
