/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amelihov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 12:45:17 by amelihov          #+#    #+#             */
/*   Updated: 2018/02/18 15:23:08 by amelihov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum1.h"

static void		print_winner(void *winner)
{
	if (winner == ai)
		ft_printf("{yellow}AI won{eoc}\n");
	else
		ft_printf("{yellow}You won{eoc}\n");
}

#define IS_VALID_CHOICE(n, b, r) ((n >= 1 && n <= 3) && b.rows[r] >= n)

static void		turn(short (*player)(struct s_board, short), t_board board,
				short curr_row)
{
	short	nmatches;

	nmatches = (*player)(board, curr_row);
	while (!IS_VALID_CHOICE(nmatches, board, curr_row))
	{
		ft_printf("{red}Wrong number of matches. Repeat again.{eoc}\n");
		nmatches = (*player)(board, curr_row);
	}
	board.rows[curr_row] -= nmatches;
	ft_printf("{cyan}%s takes %d{eoc}\n", (player == ai) ? "ai"
		: "you", nmatches);
}

#define NPLAYERS 2

static void		init_players(short (*players[NPLAYERS])(t_board, short))
{
	short	hero_is_first;
	char	buff;
	int		count;

	hero_is_first = 0;
	ft_printf("{light_blue}Who first?\nEmpty input - ai will be first{eoc}\n");
	count = 0;
	while (read(0, &buff, sizeof(buff)))
	{
		count++;
		if (buff == '\n')
			break ;
	}
	if (count > 1)
		hero_is_first = 1;
	if (hero_is_first)
	{
		players[0] = hero;
		players[1] = ai;
	}
	else
	{
		players[0] = ai;
		players[1] = hero;
	}
}

#define GAME_IS_OVER(b, r) ((r == 0 && b.rows[r] == 0) ? 1 : 0)
#define CHANGE_ROW(b, r) if (r > 0 && b.rows[r] == 0) {r--;}
#define PLAYER_NAME(p) (p == ai) ? "ai" : "hero"

void			run_game(t_board board)
{
	short	(*players[NPLAYERS])(t_board, short);
	short	curr_row;
	int		i;

	if (!(init_ai(board)))
		return ;
	init_players(players);
	curr_row = board.nrows - 1;
	while (1)
	{
		i = -1;
		while (++i < NPLAYERS)
		{
			print_board(board);
			ft_printf("{cyan}%s's turn:{eoc}\n", PLAYER_NAME(players[i]));
			turn(players[i], board, curr_row);
			CHANGE_ROW(board, curr_row);
			if (GAME_IS_OVER(board, curr_row))
			{
				print_winner(players[(NPLAYERS - 1 + i) % NPLAYERS]);
				deinit_ai();
				return ;
			}
		}
	}
}
