/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_board2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 16:56:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/02/18 14:57:52 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum1.h"

void		free_everything(t_board *tmp, t_list **head, char *tmp2, int f)
{
	if (f)
	{
		free(tmp->rows);
		ft_strdel(&tmp2);
	}
	tmp->nrows = 0;
	ft_lst_clear(head);
}

#define IN_RANGE(n) ((n) > 0 && (n) <= 10000)
#define STR_EQ(s1, s2) (ft_strlen(s1) == ft_strlen(s2))

void		allocate_from_zero(t_board *tmp, size_t count, t_list **head)
{
	size_t	count2;
	char	*tmp2;
	t_list	*tmp3;

	tmp3 = *head;
	tmp2 = NULL;
	count2 = -1;
	if (count < 1)
	{
		free_everything(tmp, head, tmp2, 0);
		return ;
	}
	tmp->rows = malloc(sizeof(short) * count);
	while ((++count2 < count) && tmp3)
	{
		tmp->rows[count2] = ft_atoi(tmp3->content);
		tmp2 = ft_itoa(tmp->rows[count2]);
		if (!IN_RANGE(tmp->rows[count2]) || !STR_EQ(tmp2, tmp3->content))
		{
			free_everything(tmp, head, tmp2, 1);
			return ;
		}
		ft_strdel(&tmp2);
		tmp3 = tmp3->next;
	}
}

void		read_from_zero(t_board *tmp, t_list **head)
{
	char	*line;
	size_t	count;

	count = 0;
	while (get_next_line(0, &line))
	{
		if (ft_strequ(line, ""))
		{
			tmp->nrows = count;
			allocate_from_zero(tmp, count, head);
			ft_strdel(&line);
			return ;
		}
		count++;
		ft_lst_push_back(head, line, ft_strlen(line));
		ft_strdel(&line);
	}
	tmp->nrows = count;
	allocate_from_zero(tmp, count, head);
}
