/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_board.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amelihov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 11:54:39 by amelihov          #+#    #+#             */
/*   Updated: 2018/02/18 17:05:31 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum1.h"

static size_t		count_num_of_n(char *file_name)
{
	char		buf;
	size_t		count;
	int			fd;

	count = 0;
	fd = open(file_name, O_RDONLY);
	if (fd == -1)
		return (0);
	while (read(fd, &buf, 1))
	{
		if (buf == '\n')
			count++;
	}
	return (count);
}

#define IN_RANGE(n) ((n) > 0 && (n) <= 10000)
#define STR_EQ(s1, s2) (ft_strlen(s1) == ft_strlen(s2))

static void			read_from_fd2_c(t_board *tmp, size_t count,
		t_list **head, char *file_name)
{
	size_t	count2;
	char	*tmp2;
	t_list	*tmp3;

	tmp2 = NULL;
	tmp3 = *head;
	if (count < 1 || tmp->nrows != count_num_of_n(file_name))
	{
		free_everything(tmp, head, tmp2, 0);
		return ;
	}
	count2 = -1;
	tmp->rows = malloc(sizeof(short) * count);
	while (++count2 < count && tmp3)
	{
		(tmp->rows)[count2] = ft_atoi(tmp3->content);
		tmp2 = ft_itoa(tmp->rows[count2]);
		if (!IN_RANGE(tmp->rows[count2]) || !STR_EQ(tmp2, tmp3->content))
		{
			free_everything(tmp, head, tmp2, 1);
			return ;
		}
		ft_strdel(&tmp2);
		tmp3 = tmp3->next;
	}
}

static void			read_from_fd2(char *file_name, t_board *tmp, t_list **head)
{
	char	*line;
	int		fd;
	size_t	count;
	int		ret;

	count = 0;
	fd = open(file_name, O_RDONLY);
	if (fd == -1)
	{
		tmp->nrows = 0;
		return ;
	}
	while ((ret = get_next_line(fd, &line)))
	{
		if (ret == -1)
			return ;
		count++;
		ft_lst_push_back(head, line, ft_strlen(line));
		ft_strdel(&line);
	}
	close(fd);
	tmp->nrows = count;
	read_from_fd2_c(tmp, count, head, file_name);
}

t_board				create_board(int argc, char *argv[], t_list **head)
{
	t_board		tmp;

	tmp.rows = NULL;
	tmp.nrows = 0;
	if (argc == 1)
		read_from_zero(&tmp, head);
	else if (argc > 2)
	{
		tmp.nrows = 0;
		return (tmp);
	}
	else
		read_from_fd2(argv[1], &tmp, head);
	return (tmp);
}
