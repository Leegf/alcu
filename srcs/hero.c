/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hero.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amelihov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 18:26:56 by amelihov          #+#    #+#             */
/*   Updated: 2018/02/18 15:32:27 by amelihov         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum1.h"

short			hero(t_board board, short curr_row)
{
	int		count;
	char	buff;
	char	value[2];

	(void)board;
	(void)curr_row;
	ft_printf("{magenta}Enter the value:{eoc} ");
	count = 0;
	while (read(0, &buff, sizeof(buff)))
	{
		count++;
		if (count == 1)
			value[0] = buff;
		else if (count == 2)
			value[1] = buff;
		if (buff == '\n')
			break ;
	}
	if (count != 2 || value[1] != '\n')
		return (-1);
	value[1] = '\0';
	return (ft_atoi(value));
}
