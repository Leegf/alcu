/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_board.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 17:17:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/02/18 12:26:43 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum1.h"

static void		print_board_2(t_board board, short max)
{
	size_t count;
	size_t count2;

	count = 0;
	while (count < board.nrows)
	{
		count2 = 0;
		write(1, "\t", 1);
		while ((int)count2 < max - board.rows[count])
		{
			count2++;
			write(1, " ", 1);
		}
		count2 = 0;
		while (count2 < (size_t)board.rows[count])
		{
			write(1, "| ", 2);
			count2++;
		}
		write(1, "\n", 1);
		count++;
	}
}

void			print_board(t_board board)
{
	size_t	count;
	short	max;

	count = 0;
	max = 0;
	while (count < board.nrows)
	{
		if (board.rows[count] > max)
			max = board.rows[count];
		count++;
	}
	print_board_2(board, max);
}
