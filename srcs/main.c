/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amelihov <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 11:45:10 by amelihov          #+#    #+#             */
/*   Updated: 2018/02/18 17:07:27 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum1.h"

static int	print_err(void)
{
	ft_putstr_fd("ERROR\n", 2);
	return (1);
}

int			main(int argc, char *argv[])
{
	t_board	board;
	t_list	*head;

	head = NULL;
	board = create_board(argc, argv, &head);
	if (!board.nrows)
		return (print_err());
	run_game(board);
	free(board.rows);
	ft_lst_clear(&head);
	return (0);
}
